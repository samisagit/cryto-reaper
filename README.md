# Crypto Reaper

A bot to monitor currency values

## Getting Started

Clone the repo where ever you so desire, move into the repo and follow the steps below.

### Prerequisites

The project relies on docker, docker-compose, an internet connection and knowledge of docker CLI tools.

```
Docker
```
```
docker-compose
```
```
interwebs
```
```
brain
```

### Running

The app looks after itself once it's running, so just start the app, and keep a beady one on it.

```
docker-compose up --build
```

or if you're a repeat customer

```
docker-compose up
```

## Running the tests

```
npm t
```

## Deployment

Much the same as the steps above to run it locally. This bot is meant for extension, it currently just logs it's output to stdout. You can abstract this to push messages to a messenger service (something fast like Redis/NATS) and fan the messages out to your decision making engines.

## Built With

* [TypeScript](https://www.typescriptlang.org/)
* [My Fingers](https://www.google.com/search?tbm=isch&source=hp&biw=1213&bih=671&ei=eXaKXLLHOsmSarjGgrAI&q=finger&oq=finger&gs_l=img.3..0l10.869.1322..1459...0.0..1.52.279.6....3..1....1..gws-wiz-img.....0..35i39.T5k6JUBF5y8)

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/samisagit/cryto-reaper/tags). 

## Authors

* **Sam White** - [samisagit](https://gitlab.com/samisagit)
