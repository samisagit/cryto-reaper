import { currencyName } from '../currency/entity'
import WebSocket = require('ws')
import stream = require('stream')

interface Ticker {
	(TickData)
}

interface TickData {
	currency: currencyName
	price: number
	date: Date
}

interface dataFeed {
	onopen: () => void
	send: (message: string, callback: (err: any) => void) => void
	onmessage: (message: string) => void
	onerror: (err: any) => void
	onclose: () => void
	readyState: number
}

class priceFeed {
	endpoint: string = 'wss://ws-feed.pro.coinbase.com'
	connection: dataFeed
	stream: stream.PassThrough
	currencies: currencyName[] = []
	restartOnClose: boolean
	subscriptionsAdded: boolean
	latency: Date

	constructor(restartOnClose: boolean) {
		this.stream = new stream.PassThrough()
		this.stream._read = () => {}	
		this.restartOnClose = restartOnClose
	}

	feed = (): stream.PassThrough => {
		return this.stream
	}

	sendSubscriptions = async (timeoutMultiplier: number = 0) => {
		let i = 1
		while (this.connection.readyState !== WebSocket.OPEN) {
			if (i > 3) {
				console.error('failed to connect to priceFeed websocket')
				return
			}
			console.log('priceFeed websocket was not ready for subscriptions')
			await ((multiplier: number) => {
				return new Promise(resolve => setTimeout(resolve, multiplier * 100));
			})(i)
			i++
		}

		let productIDs = this.currencies
			.reduce((accumulator, currentValue) => {
				return accumulator + `"${toProductID(currentValue)}",`
			}, '')
			.slice(0, -1)

		this.connection.send(
			`{
				"type": "subscribe",
				"product_ids": [
					${productIDs}
				],
				"channels": [
        				{
            					"name": "ticker",
        					"product_ids": [
							${productIDs}
  						]
       					}
				]
			}`,
			err => {
				if (typeof err == 'undefined') {
					let latencyNotice = ''
					if (typeof this.latency !== 'undefined') {
						latencyNotice = `(latency: ${new Date().getTime() -
							this.latency.getTime()}ms)`
					}
					console.log(
						'priceFeed subscriptions acknowledged',
						latencyNotice
					)
					return
				}

				console.log(err)
			}
		)
	}

	setUpConnection = () => {
		this.connection = new WebSocket(this.endpoint)
		
		this.connection.onopen = async () => {
			console.log('priceFeed websocket opened')
		}

		this.connection.onmessage = (e: any) => {
			let jobj = JSON.parse(e.data)
			if (typeof jobj.best_bid == 'undefined') {
				return
			}

			this.stream.push(
				JSON.stringify({
					price: jobj.best_bid,
					currency: fromProductID(jobj.product_id),
					date: new Date(),
				})
			)
		}

		this.connection.onerror = (e: any) => {
			console.log(`priceFeed websocket error: ${e}`)
		}

		this.connection.onclose = () => {
			this.latency = new Date()
			console.log('priceFeed websocket closed')
			if (this.restartOnClose) {
				this.connection = new WebSocket(this.endpoint)
				this.setUpConnection()
			}
		}
	}

	addSubscription = (...currencies: currencyName[]) => {
		for (let i = 0; i < currencies.length; i++) {
			this.currencies.push(currencies[i])
		}
	}
}

const toProductID = (name: currencyName): string => {
	switch (name) {
		case currencyName.BitCoin: {
			return 'BTC-GBP'
		}
		case currencyName.Etherium: {
			return 'ETH-GBP'
		}
		case currencyName.LiteCoin: {
			return 'LTC-GBP'
		}
		case currencyName.BitCoinCash: {
			return 'BCH-GBP'
		}
		case currencyName.EtheriumClassic: {
			return 'ETC-GBP'
		}
	}
}

const fromProductID = (ID: string): currencyName => {
	switch (ID) {
		case 'BTC-GBP': {
			return currencyName.BitCoin
		}
		case 'ETH-GBP': {
			return currencyName.Etherium
		}
		case 'LTC-GBP': {
			return currencyName.LiteCoin
		}
		case 'BCH-GBP': {
			return currencyName.BitCoinCash
		}
		case 'ETC-GBP': {
			return currencyName.EtheriumClassic
		}
	}
}

const toCurrencyRatio = (name: currencyName, value: number): string => {
	return `${value} ${toProductID(name).split('-')[1]} per ${
		toProductID(name).split('-')[0]
	}`
}

export { priceFeed, dataFeed, Ticker, TickData, toProductID, toCurrencyRatio }
