import {} from 'jest'
import { processor } from './entity'
import { toProductID } from './repo_coinbase'
import { knownError } from '../errors/entity'
import { currencyName } from '../currency/entity'
import stream = require('stream')

describe('processor', function() {
	it('reads from stream', function() {
		let store: string[] = []
		const procWriter = {
			write: (data: string): knownError => {
				store.push(data)
				return null
			},
		}
		const feed = new stream.PassThrough()
		const proc = new processor(feed, procWriter)
		proc.tick()

		feed.write(`{
			"currency": 1
		}`)

		expect(store.length).toBe(1)
	})

	it('seeds before writing', function() {
		let store: string[] = []
		const procWriter = {
			write: (data: string): knownError => {
				store.push(data)
				return null
			},
		}
		const feed = new stream.PassThrough()
		const proc = new processor(feed, procWriter)
		proc.tick()

		feed.write(`{
			"currency": 0
		}`)

		expect(store[0]).toBe(
			`seeding currency ${toProductID(currencyName.BitCoin)}`
		)
	})

	it('ignores same price updates', function() {
		let store: string[] = []
		const procWriter = {
			write: (data: string): knownError => {
				store.push(data)
				return null
			},
		}
		const feed = new stream.PassThrough()
		const proc = new processor(feed, procWriter)
		proc.tick()

		feed.write(`{
			"currency": 0,
			"price": "100"
		}`)
		feed.write(`{
			"currency": 0,
			"price": "100"
		}`)
		feed.write(`{
			"currency": 0,
			"price": "100"
		}`)

		expect(store.length).toBe(1)
	})

	it('logs price updates', function() {
		let store: string[] = []
		const procWriter = {
			write: (data: string): knownError => {
				store.push(data)
				return null
			},
		}
		const feed = new stream.PassThrough()
		const proc = new processor(feed, procWriter)
		proc.tick()

		feed.write(`{
			"currency": 0,
			"price": "100"
		}`)
		feed.write(`{
			"currency": 0,
			"price": "120"
		}`)
		feed.write(`{
			"currency": 0,
			"price": "100"
		}`)

		expect(store.length).toBe(3)
	})
})
