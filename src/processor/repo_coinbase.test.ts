import { priceFeed, dataFeed } from './repo_coinbase'
import { currencyName } from '../currency/entity'
import WebSocket = require('ws')

let fakeDataFeed: dataFeed = {
	onopen: () => {},
	send: (message: string) => {},
	onmessage: (message: string) => {},
	onerror: (err: any) => {},
	onclose: () => {},
	readyState: 10,
}

afterEach(() => {
	jest.clearAllMocks()
})

describe('priceFeed', () => {
	it('waits for subscriptions', async () => {
		const pf = new priceFeed(false)
		pf.connection = fakeDataFeed
		const logSpy = jest.spyOn(console, 'error')
		const sendSpy = jest.spyOn(pf.connection, 'send')
		await pf.sendSubscriptions()

		expect(console.error).toHaveBeenCalledWith(
			'failed to connect to priceFeed websocket'
		)
		expect(pf.connection.send).not.toHaveBeenCalled()
	})
	it('sends subscriptions', async () => {
		const pf = new priceFeed(false)
		pf.connection = fakeDataFeed
		pf.connection.readyState = WebSocket.OPEN
		const logSpy = jest.spyOn(console, 'error')
		const sendSpy = jest.spyOn(pf.connection, 'send')
		await pf.sendSubscriptions()

		expect(console.error).not.toHaveBeenCalled()
		expect(pf.connection.send).toHaveBeenCalled()
	})
	it('adds subscriptions', async () => {
		const pf = new priceFeed(false)
		pf.addSubscription(currencyName.BitCoin)
		
		expect(pf.currencies).toEqual([0])
	})
	it('adds more subscriptions', async () => {
		const pf = new priceFeed(false)
		pf.addSubscription(currencyName.BitCoin)
		pf.addSubscription(currencyName.Etherium)
		
		expect(pf.currencies).toEqual([0, 1])
	})
})
