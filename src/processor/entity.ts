import { currencyName } from '../currency/entity'
import {
	priceFeed,
	Ticker,
	TickData,
	toProductID,
	toCurrencyRatio,
} from './repo_coinbase'
import stream = require('stream')
import { knownError } from '../errors/entity'

interface Writer {
	write: (string) => knownError
}

interface PriceState {
	name: string
	amount: number
	updated: Date
}

class processor {
	priceStates: { [s: string]: PriceState } = {}
	stream: stream.PassThrough
	w: Writer[]

	constructor(feed: stream.PassThrough, ...writers: Writer[]) {
		this.stream = feed
		this.w = writers
	}

	tick = () => {
		this.stream.on('data', chunk => {
			const message = JSON.parse(chunk.toString())
			let state = this.priceStates[message.currency]

			// if it's the first time we've seen this currency, set state then quit
			if (typeof state === 'undefined') {
				this.output(
					`seeding currency ${toProductID(message.currency)}`
				)
				this.priceStates[message.currency] = {
					name: toProductID(message.currency),
					amount: message.price,
					updated: new Date(message.date),
				}
				return
			}

			if (state.amount !== message.price) {
				const messageDate = new Date(message.date)
				const timeDiff = messageDate.getTime() - state.updated.getTime()
				const costDiff = (message.price - state.amount).toFixed(2)
				this.output(
					`${toProductID(
						message.currency
					)} changed to ${toCurrencyRatio(
						message.currency,
						message.price
					)} (${costDiff}) after ${timeDiff}ms`
				)

				this.priceStates[message.currency].amount = message.price
				this.priceStates[message.currency].updated = messageDate
			}
		})
	}

	output = (data: string) => {
		for (let i = 0; i < this.w.length; i++) {
			this.w[i].write(data)
		}
	}
}

export { processor }
