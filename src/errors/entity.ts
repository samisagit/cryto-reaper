export enum knownError {
	thirdPartyUnavailable,
	thirdPartyRejected,
	writeError,
}
