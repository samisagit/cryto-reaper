import redis = require('redis')
import { knownError } from '../errors/entity'

class realTimeMessenger {
	conn: any
	channel: string

	constructor(channel: string) {
		this.conn = redis.createClient({
			host: 'redis',
			port: '6379',
		})

		this.channel = channel
	}

	write = (message: string): knownError => {
		this.conn.publish(this.channel, message)
		return null
	}
}

export { realTimeMessenger }
