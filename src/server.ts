'use strict'

import { currencyName } from './currency/entity'
import { processor } from './processor/entity'
import { priceFeed } from './processor/repo_coinbase'
import { knownError } from './errors/entity'
import { realTimeMessenger } from './messenger/repo_redis'

import express = require('express')

// Constants
const PORT = 9090
const HOST = '0.0.0.0'

const app = express()

app.get('/ping', function(req, res) {
	res.send('pong')
})

const rtm = new realTimeMessenger('tick')

const pf = new priceFeed(true)
pf.addSubscription(
	currencyName.BitCoin,
	currencyName.Etherium,
	currencyName.LiteCoin,
	currencyName.BitCoinCash
)
pf.setUpConnection()
pf.sendSubscriptions()

const consoleWriter = {
	write: (data: string): knownError => {
		console.log(data)
		return null
	},
}

const proc = new processor(pf.stream, consoleWriter, rtm)
proc.tick()

app.listen(PORT, HOST)
console.log(`Running on http://${HOST}:${PORT}`)
