from node:10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install && npm install typescript -g

COPY . .

RUN tsc

ENTRYPOINT ["node", "src/server.js"]

